import Axios from 'axios';
import { TOKEN_KEY } from '../actions/types';

const api = Axios.create({
  baseURL: 'https://api.mercuriosys.com'
});

api.interceptors.request.use(async config => {
  const token = localStorage.getItem(TOKEN_KEY);
  if (token) {
    config.headers.Authorization = `Bearer ${token}`;
  }
  return config;
});

export default api;
