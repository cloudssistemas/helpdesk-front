import { notification } from 'antd';

export const errorNotification = error => {
  let text;
  if (!error.response) {
    text = 'Erro no servidor';
  } else {
    text = error.response.data.message;
  }
  notification.error({
    message: 'Erro',
    description: text,
    placement: 'topLeft',
    duration: 3
  });
};
