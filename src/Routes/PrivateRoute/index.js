import React from 'react';
import { useSelector } from 'react-redux';
import { Redirect, Route } from 'react-router-dom';

export default ({ component: Component, ...rest }) => {
  const auth = useSelector(state => state.auth);

  return (
    <Route
      {...rest}
      render={props =>
        auth ? <Component {...props} {...rest} /> : <Redirect to='/login' />
      }
    />
  );
};
