import React from 'react';
import { Switch, Route } from 'react-router-dom';

import PrivateRoute from './PrivateRoute';
import Wrapper from '../components/Wrapper';
import Login from '../pages/Login';
import Home from '../pages/Home';
import Messages from '../pages/Messages';
import Reports from '../pages/Reports';
import Rates from '../pages/Rates';
import Users from '../pages/Users';

export default props => (
  <Switch>
    <Route path='/login'>
      <Login />
    </Route>
    <Route path='/'>
      <Wrapper>
        <Switch>
          <PrivateRoute exact path='/' component={Home} props={props} />
          <PrivateRoute exact path='/mensagens' component={Messages} props={props} />
          <PrivateRoute exact path='/relatorios' component={Reports} props={props} />
          <PrivateRoute exact path='/avaliacoes' component={Rates} props={props} />
          <PrivateRoute exact path='/usuarios' component={Users} props={props} />
        </Switch>
      </Wrapper>
    </Route>
  </Switch>
);
