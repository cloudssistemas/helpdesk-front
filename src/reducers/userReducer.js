import { USER } from '../actions/types';

export default (state = null, action) => {
  switch (action.type) {
    case USER:
      return action.payload;

    default:
      return state;
  }
};
