import { TOGGLE_TICKET_DETAIL } from '../actions/types';

export default (state = false, action) => {
  switch (action.type) {
    case TOGGLE_TICKET_DETAIL:
      return action.payload;

    default:
      return state;
  }
};
