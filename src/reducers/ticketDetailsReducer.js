import { TICKET_DETAILS } from '../actions/types';

export default (state = null, action) => {
  switch (action.type) {
    case TICKET_DETAILS:
      return action.payload;

    default:
      return state;
  }
};
