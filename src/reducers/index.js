import { combineReducers } from 'redux';

import authReducer from './authReducer';
import userReducer from './userReducer';
import loadingReducer from './loadingReducer';
import errorReducer from './errorReducer';
import fetchTicketsReducer from './fetchTicketsReducer';
import ticketDetailsReducer from './ticketDetailsReducer';
import detailsReducer from './detailsReducer';

export default combineReducers({
  auth: authReducer,
  user: userReducer,
  loading: loadingReducer,
  error: errorReducer,
  tickets: fetchTicketsReducer,
  ticket: ticketDetailsReducer,
  details: detailsReducer
});
