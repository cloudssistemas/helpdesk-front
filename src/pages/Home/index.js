import React from 'react';
import { PageHeader } from 'antd';

import Table from '../../components/Table';
import TicketDetails from './TicketDetails';

export default () => (
  <>
    <PageHeader title='Tickets' subTitle='Exibe uma lista de tickets' backIcon={false} />
    <Table />
    <TicketDetails />
  </>
);
