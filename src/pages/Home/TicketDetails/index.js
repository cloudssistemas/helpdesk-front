import React from 'react';
import { Drawer, Typography, Timeline } from 'antd';
import { useSelector, useDispatch } from 'react-redux';

import { toggleDetails } from '../../../actions';
import Priority from '../../../components/Priority';

export default () => {
  const ticket = useSelector(state => state.ticket);
  const details = useSelector(state => state.details);
  const dispatch = useDispatch();

  const { Title, Text } = Typography;
  const { Item } = Timeline;

  const _onClose = () => {
    dispatch(toggleDetails(false));
  };

  if (!ticket) return null;

  const events = ticket.events.map(event => (
    <Item
      key={event._id}
      label={new Date(event.updatedAt).toLocaleString('pt-BR')}
    >{`${event.event} setado para ${event.to}`}</Item>
  ));

  return (
    <Drawer
      width={640}
      title={`Ticket #${ticket.ticketId}`}
      placement='right'
      visible={details}
      onClose={_onClose}
    >
      <Priority priority={ticket.priority} />
      <Title level={2}>{ticket.subject}</Title>
      <Title level={4}>{ticket.userId.name}</Title>
      <Text>{ticket.description}</Text>
      <br />
      <Title
        level={4}
        style={{ margin: '20px 0', fontSize: '1.2em', textAlign: 'center' }}
      >
        Eventos
      </Title>
      <Timeline mode='left'>{events}</Timeline>
    </Drawer>
  );
};
