import React from 'react';
import { Layout, Row, Col, Divider, Typography } from 'antd';
import { useSelector } from 'react-redux';
import { Redirect } from 'react-router-dom';

import Form from './Form';
import styles from './styles';

export default () => {
  const { Content } = Layout;
  const { Text, Title } = Typography;
  const auth = useSelector(state => state.auth);

  if (auth) return <Redirect to='/' />;

  return (
    <Layout style={styles.background}>
      <Row>
        <Col xs={24} sm={20} md={20} lg={10} xl={8}>
          <Layout style={styles.container}>
            <Content style={styles.content}>
              <div style={styles.logo} />
              <Title style={styles.title}>Help Desk</Title>
              <Form />
              <Divider>Suporte</Divider>
              <Text>(12) 3018-2438 / (12) 99744-8778</Text>
              <br />
              <a href='mailto:contato@cloudssistemas.com.br'>
                contato@cloudssistemas.com.br
              </a>
            </Content>
          </Layout>
        </Col>
      </Row>
    </Layout>
  );
};
