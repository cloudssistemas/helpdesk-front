import {
  SIGN_IN,
  SIGN_OUT,
  TOKEN_KEY,
  USER,
  TOGGLE_LOADING,
  FETCH_TICKETS,
  TICKET_DETAILS,
  TOGGLE_TICKET_DETAIL
} from './types';
import api from '../utils/api';
import { errorNotification } from '../utils';

export const signIn = values => async dispatch => {
  const { email, password } = values;

  if (email && password) {
    await dispatch(toggleLoading(true));
    try {
      const { data } = await api.post('/signin', { email, password });
      await dispatch(user(data));
      localStorage.setItem(TOKEN_KEY, data.token);
      await dispatch({ type: SIGN_IN });
    } catch (error) {
      errorNotification(error);
    }
  }
  dispatch(toggleLoading(false));
};

export const user = data => {
  return { type: USER, payload: data };
};

export const signOut = () => {
  localStorage.removeItem(TOKEN_KEY);
  return { type: SIGN_OUT };
};

export const toggleLoading = bool => {
  return { type: TOGGLE_LOADING, payload: bool };
};

export const fetchTickets = () => async dispatch => {
  await dispatch(toggleLoading(true));
  try {
    const { data } = await api.get('/api/ticket');
    await dispatch({ type: FETCH_TICKETS, payload: data });
  } catch (error) {
    errorNotification(error);
  }
  dispatch(toggleLoading(false));
};

export const ticketDetails = id => async dispatch => {
  try {
    const { data } = await api.get(`/api/ticket/${id}`);
    console.log(data);
    await dispatch({ type: TICKET_DETAILS, payload: data });
    dispatch(toggleDetails(true));
  } catch (error) {
    errorNotification(error);
  }
};

export const toggleDetails = bool => {
  return { type: TOGGLE_TICKET_DETAIL, payload: bool };
};
