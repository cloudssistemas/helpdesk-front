import React from 'react';
import { Tag } from 'antd';

export default ({ priority }) => {
  let color;
  let text;

  switch (priority) {
    case 'urgent':
      color = 'red';
      text = 'Urgente';
      break;

    case 'high':
      color = 'orange';
      text = 'Alta';
      break;

    case 'normal':
      color = 'cyan';
      text = 'Normal';
      break;

    case 'low':
      color = 'default';
      text = 'Baixa';
      break;

    default:
      color = 'default';
      text = 'Baixa';
      break;
  }

  return (
    <Tag color={color} key={priority}>
      {text}
    </Tag>
  );
};
