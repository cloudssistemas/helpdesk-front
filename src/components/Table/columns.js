import React from 'react';

import Tag from './Tag';
import Priority from '../Priority';

export default [
  {
    title: '#ID',
    dataIndex: 'ticketId',
    key: 'ticketId',
    sorter: (a, b) => a.ticketId - b.ticketId,
    defaultSortOrder: 'ascend'
  },
  {
    title: 'Solicitante',
    dataIndex: 'userId',
    key: 'userId',
    render: user => user.name
  },
  {
    title: 'Prioridade',
    dataIndex: 'priority',
    key: 'priority',
    filters: [
      { text: 'Urgente', value: 'urgent' },
      { text: 'Alta', value: 'high' },
      { text: 'Normal', value: 'normal' },
      { text: 'Baixa', value: 'low' }
    ],
    onFilter: (value, record) => record.priority.includes(value),
    render: priority => <Priority priority={priority} />
  },
  {
    title: 'Assunto',
    dataIndex: 'subject',
    key: 'subject'
  },
  {
    title: 'Tags',
    dataIndex: 'tags',
    key: 'tags',
    render: tags => <Tag tags={tags} />
  }
];
