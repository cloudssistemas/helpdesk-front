import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Table } from 'antd';

import columns from './columns';
import { fetchTickets, ticketDetails } from '../../actions';

export default () => {
  const dispatch = useDispatch();
  const tickets = useSelector(data => data.tickets);
  const loading = useSelector(data => data.loading);

  useEffect(() => {
    dispatch(fetchTickets());
  }, [dispatch]);

  const _onRowClick = (record, rowIndex) => {
    return {
      onClick: () => {
        dispatch(ticketDetails(record._id));
      }
    };
  };

  return (
    <Table
      rowKey='ticketId'
      columns={columns}
      dataSource={tickets}
      loading={loading}
      onRow={_onRowClick}
    />
  );
};
