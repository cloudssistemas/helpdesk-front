import React from 'react';
import { Tag } from 'antd';

export default ({ tags }) => {
  const tagList = tags.map(tag => (
    <Tag color='blue' key={tag}>
      {tag}
    </Tag>
  ));

  return <span>{tagList}</span>;
};
