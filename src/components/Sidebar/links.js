export default [
  {
    id: 1,
    url: '/',
    text: 'Solicitações'
  },
  {
    id: 2,
    url: '/mensagens',
    text: 'Mensagens'
  },
  {
    id: 3,
    url: '/relatorios',
    text: 'Relatórios'
  },
  {
    id: 4,
    url: '/avaliacoes',
    text: 'Avaliações'
  },
  {
    id: 5,
    url: '/usuarios',
    text: 'Usuários'
  }
];
