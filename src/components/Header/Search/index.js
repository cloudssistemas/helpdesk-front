import React from 'react';
import { Input } from 'antd';

const { Search } = Input;

export default () => {
  return (
    <Search
      placeholder='Pesquisar tickets'
      onSearch={value => console.log(value)}
      style={{ width: '40%' }}
    />
  );
};
