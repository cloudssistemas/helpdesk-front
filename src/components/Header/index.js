import React from 'react';
import { Layout, Button } from 'antd';
import { SettingOutlined } from '@ant-design/icons';

import Search from './Search';
import Notification from './Notification';
import LogOutButton from './LogOutButton';
import styles from './styles';

export default () => {
  const { Header } = Layout;

  return (
    <Header style={styles.header}>
      <Search />
      <LogOutButton />
      <Button
        style={{ ...styles.button, marginLeft: 10, marginRight: 10 }}
        onClick={() => console.log('clicked')}
        shape='circle-outline'
        type='link'
        icon={<SettingOutlined />}
      />
      <Notification />
    </Header>
  );
};
