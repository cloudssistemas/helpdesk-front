export default {
  header: {
    background: '#fff'
  },
  button: {
    float: 'right',
    marginTop: 14
  }
};
