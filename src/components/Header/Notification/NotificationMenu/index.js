import React from 'react';
import { Menu } from 'antd';

export default () => {
  const { Item } = Menu;

  return (
    <Menu>
      <Item key='1'>Notificação 1</Item>
      <Item key='2'>Notificação 2</Item>
      <Item key='3'>Notificação 3</Item>
    </Menu>
  );
};
