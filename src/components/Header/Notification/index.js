import React, { useEffect } from 'react';
import { Badge, Button, Dropdown } from 'antd';
import { BellOutlined } from '@ant-design/icons';
import useSocket from 'use-socket.io-client';

import NotificationMenu from './NotificationMenu';

export default () => {
  const [socket] = useSocket('https://api.mercuriosys.com');

  const emitEvent = () => {
    socket.emit('signin', { id: socket.id, companyId: 1 });
  };

  useEffect(() => {
    socket.on('connect', () => console.log('Socket conectado: ', socket.id));

    socket.on('message', data => console.log(data));
  }, [socket]);

  return (
    <span style={{ float: 'right' }}>
      <Dropdown overlay={<NotificationMenu />} placement='bottomLeft' trigger={['click']}>
        <Badge count={3}>
          <Button
            onClick={emitEvent}
            shape='circle-outline'
            type='link'
            icon={<BellOutlined />}
          />
        </Badge>
      </Dropdown>
    </span>
  );
};
