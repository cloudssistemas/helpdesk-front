import React from 'react';
import { Layout } from 'antd';

import Header from '../Header';
import Sidebar from '../Sidebar';
import Panel from '../Panel';

export default ({ children }) => (
  <Layout style={{ minHeight: '100vh' }}>
    <Sidebar />
    <Layout>
      <Header />
      <Panel>{children}</Panel>
    </Layout>
  </Layout>
);
